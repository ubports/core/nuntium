module gitlab.com/ubports/development/core/nuntium

go 1.22

require (
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/jessevdk/go-flags v1.6.1 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.1.0 // indirect
	gitlab.com/ubports/development/core/go-ldm v0.0.0-20240829085643-4b8905597dac // indirect
	golang.org/x/sys v0.21.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
